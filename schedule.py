"""
To run the code
python schedule.py -c config.ini

"""
from operator import ne
import pandas as pd
import numpy as np
import pyomo.environ as pyomo
import re, os, time, argparse, configparser, string

class Schedule:
    def __init__(self,  
                 shipmentMax, shipmentMin, 
                 collectionMin,
                 freshTonnes, command,
                 elements = ['f', 'p', 'a', 'o'],
                 constrained_elements = ['f', 'p', 'a'],
                 period = 30,
                 factor = 1000,
                 price = None,
                 **kwargs):
        """
        elements: list of all elements.
        constrained_elements : list of elements on which we apply the constraints.
        kwargs: dict contains constraints of intermediate locations.
                For example, {'LB' : LB, 'LC' : LC}
        """
        #=======================
        # Constraint declaration
        #=======================
        self.factor = factor
        self.shipmentMax = shipmentMax
        self.shipmentMin = shipmentMin
        self.collectionMin = collectionMin
        self.freshTonnes = freshTonnes
        self.command = command
        self.price = price
        self.elements = elements
        self.constrained_elements = constrained_elements
        self.period = period
        self.locations = []
        for i in range(1, len(kwargs)+1):
            self.locations.insert(i, list(kwargs.keys())[i-1])
        for location in kwargs.keys():
            setattr(self, "constraint%s"%location, kwargs[location])
    
            
    def _objectiveMaximizeF(self, model):
        model.OBJ = pyomo.Objective(
            rule = lambda model:
            sum(model.MFP[(timeStep, sFP, "f")] for sFP in model.stockFP for timeStep in model.time), 
            sense = pyomo.maximize
        )
        return model
    
    def _objectiveMaximizeProfit(self, model):
        def computeProfit(model):
            profit = 0
            for timeStep in model.computingTime:
                for s in model.stockLC:
                    price = float(self.price.loc[self.price.stock.eq(s), 'price'])
                    profit += model.MfromLC[(timeStep, s)]*self.factor*price
            return profit

        model.OBJ = pyomo.Objective(
            rule = computeProfit,
            sense = pyomo.maximize
        )
        return model
    
    def _retrieveResult(self, model, objective):
        if objective == "MaximizeF":
            locations = self.locations
        elif objective == "MaximizeProfit":
            locations = self.locations[:-1]
        for currentLocation in locations:
            resultCurrentLocation = getattr(model, "M%s" % currentLocation)
            retrievedResultCurrentLocation = {(t, s, e): pyomo.value(v) for (t, s, e), v in resultCurrentLocation.items()}
            retrievedResultCurrentLocation = pd.DataFrame.from_dict(retrievedResultCurrentLocation, orient="index", columns=["value"])
            retrievedResultCurrentLocation.index = pd.MultiIndex.from_tuples(retrievedResultCurrentLocation.index, 
                                                                             names = ['time', 'stock%s' % currentLocation, 'element'])
            retrievedResultCurrentLocation.reset_index(inplace=True)
            retrievedResultCurrentLocation = retrievedResultCurrentLocation.pivot(
                values = "value", index=['time', 'stock%s' % currentLocation], columns='element'
            )
            retrievedResultCurrentLocation.columns = ['m%s' % e for e in retrievedResultCurrentLocation.columns]
            retrievedResultCurrentLocation = retrievedResultCurrentLocation.assign(
                mass = lambda x: x[['m%s' % e for e in model.element]].sum(axis = 1)
            )
            for element in model.element:
                retrievedResultCurrentLocation['g%s' % element] = retrievedResultCurrentLocation.apply(
                    lambda x: 0 if x.mass < 1/self.factor
                    else round(x['m%s' % element]/x.mass, 4), 
                    axis = 1
                )
            setattr(self, "M%s_%s" % (currentLocation, objective), retrievedResultCurrentLocation)

            if (currentLocation != locations[-1]):
                nextLocation = locations[locations.index(currentLocation) + 1]
                resultMovedMass = getattr(model, "M%sto%s" % (currentLocation, nextLocation))
                retrieveResultMovedMass = {(t, sc, sn) : pyomo.value(v) for (t, sc, sn), v in resultMovedMass.items()}
                retrieveResultMovedMass = pd.DataFrame.from_dict(retrieveResultMovedMass, orient="index", columns=["mass"])
                retrieveResultMovedMass.index = pd.MultiIndex.from_tuples(retrieveResultMovedMass.index, 
                                                                          names = ['time', 'stock%s' % currentLocation, 'stock%s' % nextLocation])
                retrieveResultMovedMass.reset_index('stock%s'%nextLocation, inplace=True)
                retrieveResultMovedMass = retrieveResultMovedMass.merge(
                    retrievedResultCurrentLocation
                    .reset_index()
                    .assign(time = lambda x: x.time.add(1))
                    .set_index(['time', 'stock%s' % currentLocation])
                    .filter(regex = "^g", axis = 1), 
                    how = 'left', left_index=True, right_index=True
                ).reset_index().set_index(['time', 'stock%s' % currentLocation, 'stock%s' % nextLocation])
                for e in self.elements:
                    retrieveResultMovedMass = retrieveResultMovedMass.assign(**{'m%s' % e: lambda x: x.mass.multiply(x['g%s' % e])})
                setattr(self, "M%sto%s_%s" % (currentLocation, nextLocation, objective), 
                        retrieveResultMovedMass[retrievedResultCurrentLocation.columns])
        
        MtoLA = {(t, s, e) : pyomo.value(v) for (t, s, e), v in model.MtoLA.items()}
        MtoLA = pd.DataFrame.from_dict(MtoLA, orient='index', columns=['value'])
        MtoLA.index = pd.MultiIndex.from_tuples(MtoLA.index, names = ['time', 'stockLA', 'element'])
        MtoLA.reset_index(inplace = True)
        MtoLA = MtoLA.pivot(
                values = "value", index=['time', 'stockLA'], columns='element'
            )
        MtoLA.columns = ["m%s" % e for e in MtoLA.columns]
        MtoLA = MtoLA.assign(mass = lambda x: x[['m%s' % e for e in model.element]].sum(axis = 1))
        for element in model.element:
            MtoLA['g%s' % element] = MtoLA.apply(
                lambda x: 0 if x.mass < 1/self.factor
                else round(x['m%s' % element]/x.mass, 4), 
                axis = 1
            )
        setattr(self, "MtoLA_%s" % objective, MtoLA)

        if objective == "MaximizeF":
            MfromFP = {(t, s) : pyomo.value(v) for (t, s), v in model.MfromFP.items()}
            MfromFP = pd.DataFrame.from_dict(MfromFP, orient='index', columns = ['mass'])
            MfromFP.index = pd.MultiIndex.from_tuples(MfromFP.index, names = ['time', 'stockFP'])
            MfromFP = MfromFP.merge(
                self.MFP_MaximizeF.reset_index()
                .assign(time = lambda x: x.time.add(1))
                .set_index(['time', 'stockFP'])
                .filter(regex = '^g', axis = 1), 
                how = 'left', left_index=True, right_index=True
            )
            for element in self.elements:
                MfromFP = MfromFP.assign(**{'m%s' % element: lambda x: x.mass.multiply(x['g%s' % element])})
            self.MfromFP_MaximizeF = MfromFP
        elif objective == "MaximizeProfit":
            MfromLC = {(t, s): pyomo.value(v) for (t,s), v in model.MfromLC.items()}
            MfromLC = pd.DataFrame.from_dict(MfromLC, orient="index", columns=['mass'])
            MfromLC.index = pd.MultiIndex.from_tuples(MfromLC.index, names=['time', 'stockLC'])
            MfromLC = MfromLC.merge(
                self.MLC_MaximizeProfit.reset_index()
                .assign(time = lambda x: x.time.add(1))
                .set_index(['time', 'stockLC'])
                .filter(regex = '^g', axis = 1), 
                how = 'left', left_index = True, right_index = True
            )
            for element in self.elements:
                MfromLC = MfromLC.assign(**{'m%s' % element: lambda x: x.mass.multiply(x['g%s' % element])})
            self.MfromLC_MaximizeProfit = MfromLC

    def _checkConstraint(self, objective):
        check = []
        if objective == "MaximizeF":
            locations = self.locations
        elif objective == "MaximizeProfit":
            locations = self.locations[:-1]
        for currentLocation in locations:
            resultLocation = getattr(self, "M%s_%s" % (currentLocation, objective))
            constraint = getattr(self, "constraint%s" % currentLocation)
            checkLocation = (resultLocation
                             .reset_index('time')
                             .merge(constraint, left_index = True, right_index = True, how = 'left')
                             .assign(check_max_capacity = lambda x: x.mass <= x.max_capacity_stock))
            checkLocation['check_mass_element'] = checkLocation.apply(
                lambda x: True if (x.mass < 1/self.factor) and (x[['m%s'%e for e in self.elements]].sum() < 1/self.factor)
                else (x.mass >= .999*x[['m%s' % e for e in self.elements]].sum())
                and (x.mass <= 1.001*x[['m%s' % e for e in self.elements]].sum()), 
                axis = 1
            )
            for element in self.constrained_elements:
                checkLocation['check_%s' % element] = checkLocation.apply(
                    lambda x: True if x.mass < 1/self.factor 
                    else (x['g%s' % element] >= x['lower_bound_%s' % element]/100)
                    & (x['g%s' % element] <= x['upper_bound_%s' % element]/100), 
                    axis = 1
                )
            check.append(checkLocation.filter(regex = 'check', axis = 1).values.all())
            if checkLocation.filter(regex = 'check', axis = 1).values.all() == False:
                print('Problem at M%s' % currentLocation)
                print(checkLocation)

            if currentLocation != locations[-1]:
                nextLocation = locations[locations.index(currentLocation) + 1]
                resultMove = getattr(self, "M%sto%s_%s" % (currentLocation, nextLocation, objective))
                checkMove = (resultMove.reset_index(['stock%s' % nextLocation])
                             .merge(resultLocation.reset_index().assign(time = lambda x: x.time+1).set_index(['time', 'stock%s' % currentLocation]), 
                                    left_index = True, right_index = True, how = 'left', suffixes = ['_move', '_prev'])
                                    .assign(mass_move_sum = lambda x: x.groupby(['time', 'stock%s' % currentLocation]).mass_move.sum()))
                checkMove['check_mass_element'] = checkMove.apply(
                    lambda x: True if (x.mass_move < 1/self.factor) and (x[['m%s_move'%e for e in self.elements]].sum() < 1/factor)
                     else (x.mass_move >= .999*x[['m%s_move' % e for e in self.elements]].sum())
                     and (x.mass_move <= 1.001*x[['m%s_move' % e for e in self.elements]].sum()), 
                    axis = 1
                )
                checkMove['check_mass'] = checkMove.apply(
                    lambda x: True if x.mass_move_sum < 1/self.factor
                    else (x.mass_move_sum <= x.mass_prev), axis = 1
                )
                for element in self.elements:
                    checkMove['check_%s' % element] = checkMove.apply(
                        lambda x: True if x.mass_move < 1/self.factor
                        else (x['g%s_move' % element] >= 0.9*x['g%s_prev' % element])
                        & (x['g%s_move' % element] <= 1.1*x['g%s_prev' % element]), 
                        axis = 1
                    )
                check.append(checkMove.filter(regex = 'check', axis = 1).values.all())
                if checkMove.filter(regex = 'check', axis = 1).values.all() == False:
                    print('Problem at M%sto%s' % (currentLocation, nextLocation))
                    print(checkMove)

        checkToLA = (getattr(self, "MtoLA_%s" % objective)
                     .merge(self.freshTonnes.reset_index()
                            .melt(value_vars = self.freshTonnes.columns, 
                                  id_vars = ['time'], 
                                  value_name = 'freshTonnes', 
                                  var_name = 'stockLA')
                            .set_index(['time', 'stockLA']), 
                            how = 'left', left_index=True, right_index=True))
        checkToLA['check_mass_element'] = checkToLA.apply(
            lambda x: True if (x.mass < 1/self.factor) and (x[['m%s' % e for e in self.elements]].sum() < 1/self.factor)
            else (x.mass >= .999*x[['m%s' % e for e in self.elements]].sum())
            and (x.mass <= 1.001*x[['m%s' % e for e in self.elements]].sum()), 
            axis = 1
        )   
        checkToLA['check_freshTonnes'] = checkToLA.apply(
            lambda x: True if ((x.mass < 1/self.factor) & (x.freshTonnes < 1/self.factor))
            else (x.mass >= 0.9*x.freshTonnes) & (x.mass <= 1.1*x.freshTonnes), 
            axis = 1
        )         
        check.append(checkToLA.filter(regex='check', axis = 1).values.all())
        if checkToLA.filter(regex = 'check', axis = 1).values.all() == False:
            print('Problem at MtoLA')
            print(checkToLA)

        if objective == "MaximizeF":
            checkFromFP = (self.MfromFP_MaximizeF
                           .merge(self.MFP_MaximizeF.reset_index()
                                  .assign(time = lambda x: x.time + 1)
                                  .set_index(['time', 'stockFP']), 
                                  how = 'left', left_index = True, 
                                  right_index = True, suffixes = ['_move', '_prev'])
                           .merge(self.MfromFP_MaximizeF.reset_index().groupby('time').mass.sum().to_frame(), 
                                  how = 'left', left_index = True, right_index = True))
            checkFromFP['check_mass_element'] = checkFromFP.apply(
                lambda x: True if (x.mass_move < 1/self.factor) and (x[['m%s_move' % e for e in self.elements]].sum() < 1/self.factor)
                else (x.mass_move >= .999*x[['m%s_move' % e for e in self.elements]].sum())
                and (x.mass_move <= 1.001*x[['m%s_move' % e for e in self.elements]].sum()), 
                axis = 1
            )
            checkFromFP['check_mass'] = checkFromFP.apply(
                lambda x: True if ((x.mass < 1/self.factor) & (x.mass_prev < 1/self.factor))
                else (x.mass >= 0.9*self.collectionMin) & (x.mass <= 1.1*self.collectionMin), 
                axis = 1
            )
            for element in self.elements:
                checkFromFP["check_g%s" % element] = checkFromFP.apply(
                    lambda x: True if x.mass < 1/self.factor
                    else (x['g%s_move' % element] >= 0.9*x['g%s_prev' % element])
                    & (x['g%s_move' % element] <= 1.1*x['g%s_prev' % element]), 
                    axis = 1
                )
            check.append(checkFromFP.filter(regex = 'check', axis = 1).values.all())
            if checkFromFP.filter(regex='check', axis = 1).values.all() == False:
                print('Problem at MfromFP')
                print(checkFromFP)
        elif objective == "MaximizeProfit":
            checkFromLC = (self.MfromLC_MaximizeProfit
                           .merge(self.MLC_MaximizeProfit.reset_index()
                                  .assign(time = lambda x: x.time + 1)
                                  .set_index(['time', 'stockLC']), 
                                  how = 'left', left_index = True, 
                                  right_index = True, suffixes = ['_move', '_prev'])
                           .merge(self.MfromLC_MaximizeProfit.reset_index().groupby('time').mass.sum().to_frame(), 
                                  how = 'left', left_index = True, right_index = True))
            checkFromLC['check_mass_element'] = checkFromLC.apply(
                lambda x: True if (x.mass_move < 1/self.factor) and (x[['m%s_move' % e for e in self.elements]].sum() < 1/self.factor)
                else (x.mass_move >= .999*x[['m%s_move' % e for e in self.elements]].sum())
                and (x.mass_move <= 1.001*x[['m%s_move' % e for e in self.elements]].sum()), 
                axis = 1
            )
            checkFromLC['check_mass'] = checkFromLC.apply(
                lambda x: True if ((x.mass < 1/self.factor) & (x.mass_prev < 1/self.factor))
                else (x.mass >= 0.9*self.collectionMin) & (x.mass <= 1.1*self.collectionMin), 
                axis = 1
            )
            for element in self.elements:
                checkFromLC["check_g%s" % element] = checkFromLC.apply(
                    lambda x: True if x.mass < 1/self.factor
                    else (x['g%s_move' % element] >= 0.9*x['g%s_prev' % element])
                    & (x['g%s_move' % element] <= 1.1*x['g%s_prev' % element]), 
                    axis = 1
                )
            check.append(checkFromLC.filter(regex = 'check', axis = 1).values.all())
            if checkFromLC.filter(regex='check', axis = 1).values.all() == False:
                print('Problem at MFromLC')
                print(checkFromLC)

        return all(check)
    
    def _makeOutputFile(self, outputFileName, outputDirectory, objective):
        writer = pd.ExcelWriter(os.path.join(outputDirectory, outputFileName), engine = "xlsxwriter")
        if objective == "MaximizeF":
            locations = self.locations
        elif objective == "MaximizeProfit":
            locations = self.locations[:-1]
        for currentLocation in locations:
            massLocation = getattr(self, "M%s_%s" % (currentLocation, objective))
            for column in massLocation.columns:
                if column[0] == 'm':
                    massLocation = massLocation.assign(**{column: lambda x: round(x[column]*self.factor, 2)})
                elif column[0] == 'g':
                    massLocation = massLocation.assign(**{column: lambda x: x[column]*100})
            massLocation[['m%s' % e for e in self.elements] 
                         + ['mass']
                         + ['g%s' % e for e in self.elements]].to_excel(writer, sheet_name = "Location %s" % currentLocation.removeprefix('L'))
            if currentLocation == "LA":
                nextLocation = self.locations[1]
                massTransport = getattr(self, "MLAto%s_%s" % (nextLocation, objective))
                for column in massTransport.columns:
                    if column[0] == 'm':
                        massTransport = massTransport.assign(**{column: lambda x: round(x[column]*self.factor, 2)})
                    elif column[0] == 'g':
                        massTransport = massTransport.assign(**{column: lambda x: x[column]*100})
                massTransport[['m%s' % e for e in self.elements] 
                         + ['mass']
                         + ['g%s' % e for e in self.elements]].to_excel(writer, sheet_name = "Transport A to %s" % nextLocation.removeprefix('L'))
            elif currentLocation != locations[-1]:
                nextLocation = locations[locations.index(currentLocation) + 1]
                massTransport = getattr(self, "M%sto%s_%s" % (currentLocation, nextLocation, objective))
                for column in massTransport.columns:
                    if column[0] == 'm':
                        massTransport = massTransport.assign(**{column : lambda x: round(x[column]*self.factor, 2)})
                    elif column[0] == 'g':
                        massTransport = massTransport.assign(**{column: lambda x: x[column]*100})
                (massTransport[['m%s' % e for e in self.elements] 
                     + ['mass']
                     + ['g%s' % e for e in self.elements]]
                 .to_excel(writer, 
                           sheet_name = "Transport %s to %s" 
                           % (currentLocation.removeprefix('L'), 
                              nextLocation.removeprefix('L'))))
        MtoLA = getattr(self,"MtoLA_%s" % objective)
        if objective == "MaximizeF":
            collection = getattr(self, "MfromFP_MaximizeF")
        elif objective == "MaximizeProfit":
            collection = getattr(self, "MfromLC_MaximizeProfit")
        for column in MtoLA.columns:
            if column[0] == 'm':
                MtoLA = MtoLA.assign(**{column: lambda x : round(x[column]*self.factor, 2)})
                collection = collection.assign(**{column: lambda x : round(x[column]*self.factor, 2)})
            elif column[0] == 'g':
                MtoLA = MtoLA.assign(**{column: lambda x: x[column]*100})
                collection = collection.assign(**{column: lambda x: x[column]*100})
        MtoLA[['m%s' % e for e in self.elements] 
                         + ['mass']
                         + ['g%s' % e for e in self.elements]].to_excel(writer, sheet_name = "Distribution fresh tonnes A")
        if objective == "MaximizeF":
            collection[['m%s' % e for e in self.elements] 
                             + ['mass']
                             + ['g%s' % e for e in self.elements]].to_excel(writer, sheet_name = "Collection quantity")
        elif objective == "MaximizeProfit":
            collection = (collection.merge(self.price.set_index('stock').rename_axis('stockLC'), 
                                           how = 'left', left_index = True, right_index = True)
                                    .assign(profit = lambda x: x.mass.multiply(x.price)))
            collection[['m%s' % e for e in self.elements]
                       + ['mass']
                       + ['g%s' % e for e in self.elements]
                       + ['profit']].to_excel(writer, sheet_name = "Collection quantity")
        writer.close()

    def _launchOptimization(self, objective, init = False, 
                            firstTime = 1, lastTime = 30, 
                            max_iter = 1000000, tol = 1E-5, **kwargs):
        model = pyomo.ConcreteModel()
        model.time = pyomo.Set(initialize = range(firstTime - 1, lastTime + 1))
        model.computingTime = pyomo.Set(initialize = range(firstTime, lastTime + 1))
        model.element = pyomo.Set(initialize = self.elements)
        model.constrainedElement = pyomo.Set(initialize = self.constrained_elements)

        model.stockLA = pyomo.Set(initialize = self.constraintLA.index.to_list())
        model.stockLB = pyomo.Set(initialize = self.constraintLB.index.to_list())
        model.stockLC = pyomo.Set(initialize = self.constraintLC.index.to_list())    
        model.stockFP = pyomo.Set(initialize = self.constraintFP.index.to_list())

        def initMassLA(model, timeStep, stockLA, element):
            if init == False :
                if timeStep == firstTime-1:
                    return kwargs['LA'].loc[(timeStep,stockLA),'m%s'%element]
                elif self.freshTonnes.loc[timeStep, stockLA] == 0:
                    return 0
                elif element == 'f':
                    return (self.collectionMin
                            /self.freshTonnes.loc[timeStep].gt(0).sum()
                            *self.constraintLA.loc[stockLA, 'upper_bound_f']/100)
    
                elif element in model.constrainedElement:
                    return self.collectionMin/self.freshTonnes.loc[timeStep].gt(0).sum()*self.constraintLA.loc[stockLA, 'lower_bound_%s' % element]/100
                else:
                    return self.collectionMin/self.freshTonnes.loc[timeStep].gt(0).sum()*(1 - (self.constraintLA.filter(regex='^lower', axis=1).loc[stockLA]/100).sum())
            else:
                return getattr(self, "MLA_%s" % objective).loc[(timeStep, stockLA), 'm%s' % element]
        model.MLA = pyomo.Var(model.time, 
                              model.stockLA, 
                              model.element, 
                              domain = pyomo.NonNegativeReals, 
                              initialize = initMassLA)
        for sLA in model.stockLA:
            for e in model.element:
                model.MLA[(firstTime-1,sLA,e)].fix(kwargs['LA'].loc[(firstTime-1,sLA),'m%s'%e])

        def initMassLB(model, timeStep, stockLB, element):
            if init == False :
                if timeStep < 1: 
                    return 0
                elif element == 'f':
                    return self.collectionMin/len(model.stockLB)*self.constraintLB.loc[stockLB, 'upper_bound_f']/100
                elif self.constraintLB.loc[stockLB, 'upper_bound_f'] < 100:
                    if element in model.constrainedElement:
                        return self.collectionMin/len(model.stockLB)*self.constraintLB.loc[stockLB, 'lower_bound_%s' % element]/100
                    else:
                        return (1-sum(self.constraintLB.loc[stockLB, 'lower_bound_%s' % e]/100 if (e != 'f')
                                             else self.constraintLB.loc[stockLB, 'upper_bound_f']/100
                                             for e in model.constrainedElement))*self.collectionMin/len(model.stockLB)
                else:
                    return 0
            else:
                return getattr(self, "MLB_%s" % objective).loc[(timeStep, stockLB), 'm%s' % element]
        model.MLB = pyomo.Var(model.time, 
                              model.stockLB, 
                              model.element, 
                              domain = pyomo.NonNegativeReals, 
                              initialize = initMassLB)
        for t in model.time:
            if t < 1:
                for sLB in model.stockLB:
                    for e in model.element:
                        model.MLB[(t, sLB, e)].fix(0)
            elif t == firstTime-1:
                for sLB in model.stockLB:
                    for e in model.element:
                        model.MLB[(t, sLB, e)].fix(kwargs['LB'].loc[(t, sLB), 'm%s' % e])

        def initMassLC(model, timeStep, stockLC, element):
            if init == False:
                if timeStep < 2: 
                    return 0
                elif element == 'f':
                    return self.collectionMin/len(model.stockLC)*self.constraintLC.loc[stockLC, 'upper_bound_f']/100
                elif self.constraintLC.loc[stockLC, 'upper_bound_f'] < 100:
                    if element in model.constrainedElement:
                        return self.collectionMin/len(model.stockLC)*self.constraintLC.loc[stockLC, 'lower_bound_%s' % element]/100
                    else:
                        return (1-sum(self.constraintLC.loc[stockLC, 'lower_bound_%s' % e]/100 if (e != 'f')
                                             else self.constraintLC.loc[stockLC, 'upper_bound_f']/100
                                             for e in model.constrainedElement))*self.collectionMin/len(model.stockLC)
                else:
                    return 0
            else:
                return getattr(self,"MLC_%s"% objective).loc[(timeStep, stockLC), 'm%s' % element]
        model.MLC = pyomo.Var(model.time, 
                              model.stockLC, 
                              model.element, 
                              domain = pyomo.NonNegativeReals, 
                              initialize = initMassLC)
        for t in model.time:
            if t < 2:
                for sLC in model.stockLC:
                    for e in model.element:
                        model.MLC[(t, sLC, e)].fix(0)
            elif t == firstTime-1:
                for sLC in model.stockLC:
                    for e in model.element:
                        model.MLC[(t, sLC, e)].fix(kwargs['LC'].loc[(t, sLC), 'm%s' % e])
        
        if objective == "MaximizeF":
            def initMassFP(model, timeStep, stockFP, element):
                if init == False:
                    if timeStep < 3: 
                        return 0
                    elif element == 'f':
                        return self.collectionMin/len(model.stockFP)*self.constraintFP.loc[stockFP, 'upper_bound_f']/100
                    elif self.constraintFP.loc[stockFP, 'upper_bound_f'] < 100:
                        if element in model.constrainedElement:
                            return self.collectionMin/len(model.stockFP)*self.constraintFP.loc[stockFP, 'lower_bound_%s' % element]/100
                        else:
                            return (1-sum(self.constraintFP.loc[stockFP, 'lower_bound_%s' % e]/100 if (e != 'f')
                                                 else self.constraintFP.loc[stockFP, 'upper_bound_f']/100
                                                 for e in model.constrainedElement))*self.collectionMin/len(model.stockFP)
                    else:
                        return 0
                else:
                    return getattr(self, "MFP_%s" % objective).loc[(timeStep, stockFP), 'm%s' % element]
            model.MFP = pyomo.Var(model.time, 
                                  model.stockFP, 
                                  model.element, 
                                  domain = pyomo.NonNegativeReals, 
                                  initialize = initMassFP)
            for t in model.time:
                if t < 3:
                    for sFP in model.stockFP:
                        for e in model.element:
                            model.MFP[(t, sFP, e)].fix(0)
                elif t == firstTime-1:
                    for sFP in model.stockFP:
                        for e in model.element:
                            model.MFP[(t, sFP, e)].fix(kwargs['FP'].loc[(t, sFP), 'm%s' % e])


        def initMLAtoLB(model, timeStep, stockLA, stockLB):
            if init == False:
                if timeStep == firstTime:
                    StockLA = kwargs['LA'][kwargs['LA'].sum(axis = 1).gt(0)].loc[firstTime-1].index
                    nStockLA = len(StockLA)
                else:
                    StockLA = [sLA for sLA in model.stockLA if pyomo.value(model.MLA[(timeStep-1, sLA, 'f')]) > 0]
                    nStockLA = len(StockLA)
                nStockLB = len(model.stockLB)
                if stockLA in StockLA:
                    return min(self.collectionMin/nStockLA/nStockLB, pyomo.value(sum(model.MLA[(timeStep-1, stockLA, e)] for e in model.element)))
                else:
                    return 0
            else:
                return getattr(self, "MLAtoLB_%s" % objective).loc[(timeStep, stockLA, stockLB), 'mass']
        model.MLAtoLB = pyomo.Var(model.computingTime, model.stockLA, model.stockLB, 
                                  domain = pyomo.NonNegativeReals, 
                                  initialize = initMLAtoLB)

        def initMLBtoLC(model, timeStep, stockLB, stockLC):
            if init == False:
                if timeStep < 2:
                    return 0
                else:
                    if timeStep == firstTime:
                        StockLB = kwargs['LB'][kwargs['LB'].sum(axis = 1).gt(0)].loc[firstTime-1].index
                        nStockLB = len(StockLB)
                    else:
                        StockLB = [sLB for sLB in model.stockLB if pyomo.value(model.MLB[(timeStep-1, sLB, 'f')]) > 0]
                        nStockLB = len(StockLB)
                    nStockLC = len(model.stockLC)
                    if stockLB in StockLB:
                        return min(self.collectionMin/nStockLB/nStockLC, pyomo.value(sum(model.MLB[(timeStep-1, stockLB, e)] for e in model.element)))
                    else:
                        return 0
            else:
                return getattr(self, "MLBtoLC_%s" % objective).loc[(timeStep, stockLB, stockLC), 'mass']
        model.MLBtoLC = pyomo.Var(model.computingTime, model.stockLB, model.stockLC, 
                                  domain = pyomo.NonNegativeReals, 
                                  initialize = initMLBtoLC)
        for t in model.computingTime:
            if t < 2:
                for sLB in model.stockLB:
                    for sLC in model.stockLC:
                        model.MLBtoLC[(t, sLB, sLC)].fix(0)

        if objective == "MaximizeF":
            def initMLCtoFP(model, timeStep, stockLC, stockFP):
                if init == False:
                    if timeStep < 3:
                        return 0
                    else:
                        if timeStep == firstTime:
                            StockLC = kwargs['LC'][kwargs['LC'].sum(axis = 1).gt(0)].loc[firstTime-1].index
                            nStockLC = len(StockLC)
                        else:
                            StockLC = [sLC for sLC in model.stockLC if pyomo.value(model.MLC[(timeStep-1, sLC, 'f')]) > 0]
                            nStockLC = len(StockLC)
                        nStockFP = len(model.stockFP)
                        if stockLC in StockLC:
                            return min(self.collectionMin/nStockLC/nStockFP, pyomo.value(sum(model.MLC[(timeStep-1, stockLC, e)] for e in model.element)))
                        else:
                            return 0
                else:
                    return getattr(self, "MLCtoFP_%s" % objective).loc[(timeStep, stockLC, stockFP), 'mass']
            model.MLCtoFP = pyomo.Var(model.computingTime, model.stockLC, model.stockFP, 
                                      domain = pyomo.NonNegativeReals, 
                                      initialize = initMLCtoFP)
            for t in model.computingTime:
                if t < 3:
                    for sLC in model.stockLC:
                        for sFP in model.stockFP:
                            model.MLCtoFP[(t, sLC, sFP)].fix(0)
            
            model.MfromFP = pyomo.Var(model.computingTime, 
                                      model.stockFP, 
                                      domain = pyomo.NonNegativeReals, 
                                      initialize = lambda model, timeStep, stockFP:
                                      self.MfromFP_MaximizeF.loc[(timeStep, stockFP), 'mass'] if init
                                      else self.collectionMin/len(model.stockFP))
            for t in model.computingTime:
                if t < 4:
                    for sFP in model.stockFP:
                        model.MfromFP[(t,sFP)].fix(0)


        def initMtoLA(model, timeStep, stockLA, element):
            if init ==  False:
                if element == 'f':
                    return self.constraintLA.loc[stockLA, 'upper_bound_f']/100*self.freshTonnes.loc[timeStep, stockLA]
                elif element in model.constrainedElement:
                    return self.constraintLA.loc[stockLA, 'lower_bound_%s' % element]/100*self.freshTonnes.loc[timeStep, stockLA]
                else:
                    return (1-sum([self.constraintLA.loc[stockLA, 'lower_bound_%s' % e]/100 if (e != 'f') 
                                   else self.constraintLA.loc[stockLA, 'upper_bound_f']/100 
                                   for e in model.constrainedElement]))*self.freshTonnes.loc[timeStep, stockLA]
            else:
                return self.MtoLA.loc[(timeStep, stockLA), 'm%s' % element]
        model.MtoLA = pyomo.Var(model.computingTime, model.stockLA, model.element, 
                                domain = pyomo.NonNegativeReals, 
                                initialize = initMtoLA)

        if objective == "MaximizeProfit":
            model.MfromLC = pyomo.Var(model.computingTime, model.stockLC, 
                                      domain = pyomo.NonNegativeReals, 
                                      initialize = lambda model, timeStep, stockLC:
                                      self.MfromLC_MaximizeProfit.loc[(timeStep, stockLC)] if init
                                      else self.collectionMin/len(model.stockLC))

        model.ConstraintGradeUpperLA = pyomo.Constraint(
            model.time, model.stockLA, model.constrainedElement, 
            rule = lambda model, timeStep, stockLA, element:
            pyomo.Constraint.Skip if pyomo.value(sum(model.MLA[(timeStep, stockLA,e)] for e in model.element)) == 0
            else model.MLA[(timeStep,stockLA,element)]/sum(model.MLA[(timeStep,stockLA,e)] for e in model.element) <= self.constraintLA.loc[stockLA,'upper_bound_%s'%element]/100
        )
        model.ConstraintGradeLowerLA = pyomo.Constraint(
            model.time, model.stockLA, model.constrainedElement, 
            rule = lambda model, timeStep, stockLA, element:
            pyomo.Constraint.Skip if pyomo.value(sum(model.MLA[(timeStep, stockLA, e)] for e in model.element)) == 0
            else model.MLA[(timeStep, stockLA, element)]/sum(model.MLA[(timeStep, stockLA, e)] for e in model.element) >= self.constraintLA.loc[stockLA, 'lower_bound_%s'%element]/100
        )
        model.ConstraintGradeUpperLB = pyomo.Constraint(
            model.computingTime, model.stockLB, model.constrainedElement, 
            rule = lambda model, timeStep, stockLB, element:
            pyomo.Constraint.Skip if pyomo.value(sum(model.MLB[(timeStep, stockLB, e)] for e in model.element)) == 0
            else model.MLB[(timeStep,stockLB,element)]/sum(model.MLB[(timeStep, stockLB, e)] for e in model.element) <= self.constraintLB.loc[stockLB,'upper_bound_%s'%element]/100
        )
        model.ConstraintGradeLowerLB = pyomo.Constraint(
            model.computingTime, model.stockLB, model.constrainedElement, 
            rule = lambda model, timeStep, stockLB, element:
            pyomo.Constraint.Skip if pyomo.value(sum(model.MLB[(timeStep, stockLB, e)] for e in model.element)) == 0
            else model.MLB[(timeStep, stockLB, element)]/sum(model.MLB[(timeStep, stockLB, e)] for e in model.element) >= self.constraintLB.loc[stockLB, 'lower_bound_%s'%element]/100
        )
        model.ConstraintGradeUpperLC = pyomo.Constraint(
            model.computingTime, model.stockLC, model.constrainedElement, 
            rule = lambda model, timeStep, stockLC, element:
            pyomo.Constraint.Skip if pyomo.value(sum(model.MLC[(timeStep, stockLC, e)] for e in model.element)) == 0
            else model.MLC[(timeStep,stockLC,element)]/sum(model.MLC[(timeStep, stockLC, e)] for e in model.element) <= self.constraintLC.loc[stockLC,'upper_bound_%s'%element]/100
        )
        model.ConstraintGradeLowerLC = pyomo.Constraint(
            model.computingTime, model.stockLC, model.constrainedElement, 
            rule = lambda model, timeStep, stockLC, element:
            pyomo.Constraint.Skip if pyomo.value(sum(model.MLC[(timeStep, stockLC, e)] for e in model.element)) == 0
            else model.MLC[(timeStep, stockLC, element)]/sum(model.MLC[(timeStep, stockLC, e)] for e in model.element) >= self.constraintLC.loc[stockLC, 'lower_bound_%s'%element]/100
        )
        if objective == "MaximizeF":
            model.ConstraintGradeUpperFP = pyomo.Constraint(
                model.computingTime, model.stockFP, model.constrainedElement, 
                rule = lambda model, timeStep, stockFP, element:
                pyomo.Constraint.Skip if pyomo.value(sum(model.MFP[(timeStep, stockFP, e)] for e in model.element)) == 0
                else model.MFP[(timeStep,stockFP,element)]/sum(model.MFP[(timeStep, stockFP, e)] for e in model.element) <= self.constraintFP.loc[stockFP,'upper_bound_%s'%element]/100
            )
            model.ConstraintGradeLowerFP = pyomo.Constraint(
                model.computingTime, model.stockFP, model.constrainedElement, 
                rule = lambda model, timeStep, stockFP, element:
                pyomo.Constraint.Skip if pyomo.value(sum(model.MFP[(timeStep, stockFP, e)] for e in model.element)) == 0
                else model.MFP[(timeStep, stockFP, element)]/sum(model.MFP[(timeStep, stockFP, e)] for e in model.element) >= self.constraintFP.loc[stockFP, 'lower_bound_%s'%element]/100
            )

        model.ConstraintMaxStockLA = pyomo.Constraint(
            model.time, model.stockLA, 
            rule = lambda model, t, s:
            sum(model.MLA[(t, s, e)] for e in model.element) 
            <= self.constraintLA.loc[s, 'max_capacity_stock']
        )
        model.ConstraintMaxStockLB = pyomo.Constraint(
            model.time, model.stockLB, 
            rule = lambda model, t, s:
            sum(model.MLB[(t, s, e)] for e in model.element) 
            <= self.constraintLB.loc[s, 'max_capacity_stock']
        )
        model.ConstraintMaxStockLC = pyomo.Constraint(
            model.time, model.stockLC, 
            rule = lambda model, t, s:
            sum(model.MLC[(t, s, e)] for e in model.element) 
            <= self.constraintLC.loc[s, 'max_capacity_stock']
        )
        if objective == "MaximizeF":
            model.ConstraintMaxStockFP = pyomo.Constraint(
                model.time, model.stockFP, 
                rule = lambda model, t, s:
                sum(model.MFP[(t, s, e)] for e in model.element) 
                <= self.constraintFP.loc[s, 'max_capacity_stock']
                )

        model.ConstraintMassLAtoLB = pyomo.Constraint(
            model.computingTime, model.stockLA,
            rule = lambda model, timeStep, stockLA:
            sum(model.MLAtoLB[(timeStep, stockLA, sLB)] for sLB in model.stockLB)
            <= sum(model.MLA[(timeStep - 1, stockLA, e)] for e in model.element)
        )
        model.ConstraintMassLBtoLC = pyomo.Constraint(
            model.computingTime, model.stockLB,
            rule = lambda model, timeStep, stockLB:
            sum(model.MLBtoLC[(timeStep, stockLB, sLC)] for sLC in model.stockLC) 
            <= sum(model.MLB[(timeStep - 1, stockLB, e)] for e in model.element)
        )
        if objective == "MaximizeF":
            model.ConstraintMassLCtoFP = pyomo.Constraint(
                model.computingTime, model.stockLC,
                rule = lambda model, timeStep, stockLC:
                sum(model.MLCtoFP[(timeStep, stockLC, sFP)] for sFP in model.stockFP) 
                <= sum(model.MLC[(timeStep - 1, stockLC, e)] for e in model.element)
            )


        model.ConstraintTransportCapacityLAtoLB = pyomo.Constraint(
            model.computingTime, 
            rule = lambda model, timeStep:
            (self.shipmentMin, 
            sum(model.MLAtoLB[(timeStep, sLA, sLB)] 
                for sLA in model.stockLA for sLB in model.stockLB), 
            self.shipmentMax)
        )
        model.ConstraintTransportCapacityLBtoLC = pyomo.Constraint(
            model.computingTime, 
            rule = lambda model, timeStep:
            (self.shipmentMin, 
            sum(model.MLBtoLC[(timeStep, sLB, sLC)] 
                 for sLB in model.stockLB for sLC in model.stockLC), 
            self.shipmentMax)
            )
        if objective == "MaximizeF":
            model.ConstraintTransportCapacityLCtoFP = pyomo.Constraint(
                model.computingTime, 
                rule = lambda model, timeStep:
                (self.shipmentMin, 
                sum(model.MLCtoFP[(timeStep, sLC, sFP)] 
                    for sLC in model.stockLC for sFP in model.stockFP), 
                self.shipmentMax)
            )

        model.ConstraintMassToLA = pyomo.Constraint(
            model.computingTime, model.stockLA, 
            rule = lambda model, timeStep, stockLA:
            sum(model.MtoLA[(timeStep, stockLA, e)] for e in model.element) 
            == self.freshTonnes.loc[timeStep, stockLA]
        )
        if objective == "MaximizeF":
            model.ConstraintMassFromFP = pyomo.Constraint(
                model.computingTime, model.stockFP, 
                rule = lambda model, timeStep, stockFP:
                model.MfromFP[(timeStep, stockFP)] 
                <= sum(model.MFP[(timeStep-1, stockFP, e)] for e in model.element)
            )
            model.ConstraintCollectionMin = pyomo.Constraint(
                model.computingTime, 
                rule = lambda model, timeStep:
                pyomo.Constraint.Skip if timeStep < 4
                else (sum(model.MfromFP[(timeStep, sFP)] for sFP in model.stockFP) 
                    == self.collectionMin)
            )
        elif objective == "MaximizeProfit":
            model.ConstraintMassFromLC = pyomo.Constraint(
                model.computingTime, model.stockLC, 
                rule = lambda model, timeStep, stockLC:
                model.MfromLC[(timeStep, stockLC)]
                <= sum(model.MLC[(timeStep-1, stockLC, e)] for e in model.element)
            )
            model.ConstraintCollectionMin = pyomo.Constraint(
                model.computingTime, 
                rule = lambda model, timeStep:
                pyomo.Constraint.Skip if timeStep < 3
                else (sum(model.MfromLC[(timeStep, sLC)] for sLC in model.stockLC)
                      == self.collectionMin)
            )
    

        def computeMassLA(model, timeStep, stockLA, element):
            if pyomo.value(sum(model.MLA[(timeStep-1, stockLA, e)] 
                            for e in model.element)) == 0:
                return (model.MLA[(timeStep-1, stockLA, element)] 
                        + model.MtoLA[(timeStep, stockLA, element)])
            else:
                return (model.MLA[(timeStep-1, stockLA, element)] 
                        - sum(model.MLAtoLB[(timeStep, stockLA, sLB)] 
                              for sLB in model.stockLB)
                        *model.MLA[(timeStep-1, stockLA, element)]
                        /sum(model.MLA[(timeStep-1, stockLA, e)] for e in model.element)
                        + model.MtoLA[(timeStep, stockLA, element)])

        model.ConstraintMassLA = pyomo.Constraint(
            model.computingTime, model.stockLA, model.element, 
            rule = lambda model, timeStep, stockLA, element:
            computeMassLA(model, timeStep, stockLA, element) - model.MLA[(timeStep, stockLA, element)] == 0
        )
    
        def computeMassLB(model, timeStep, stockLB, element):
            if (timeStep < 2) or (pyomo.value(sum(model.MLB[(timeStep-1, stockLB, e)] 
                                                for e in model.element)) == 0):
                MLBtoLC = 0
            else:
                MLBtoLC = (sum(model.MLBtoLC[(timeStep, stockLB, sLC)] 
                              for sLC in model.stockLC)
                            *model.MLB[(timeStep-1, stockLB, element)]
                              /sum(model.MLB[(timeStep-1, stockLB, e)] 
                                   for e in model.element))
            MLAtoLB = 0
            for sLA in model.stockLA:
                if pyomo.value(sum(model.MLA[(timeStep-1, sLA, e)] for e in model.element)) != 0:
                    MLAtoLB += (model.MLAtoLB[(timeStep, sLA, stockLB)]
                                *model.MLA[(timeStep-1, sLA, element)]
                                /sum(model.MLA[(timeStep-1, sLA, e)] 
                                     for e in model.element))
            return (model.MLB[(timeStep-1, stockLB, element)]
                    - MLBtoLC
                    + MLAtoLB)
        model.ConstraintMassLB = pyomo.Constraint(
            model.computingTime, model.stockLB, model.element, 
            rule = lambda model, timeStep, stockLB, element:
            computeMassLB(model, timeStep, stockLB, element) 
            == model.MLB[(timeStep, stockLB, element)]
        )

        if objective == "MaximizeF":
            def computeMassLC(model, timeStep, stockLC, element):
                if (timeStep < 3) or (pyomo.value(sum(model.MLC[(timeStep-1, stockLC, e)] 
                                                  for e in model.element)) == 0):
                    MLCtoFP = 0
                else:
                    MLCtoFP = (sum(model.MLCtoFP[(timeStep, stockLC, sFP)] 
                                  for sFP in model.stockFP)
                                  *model.MLC[(timeStep-1, stockLC, element)]
                                /sum(model.MLC[(timeStep-1, stockLC, e)] 
                                       for e in model.element))
                MLBtoLC = 0
                for sLB in model.stockLB:
                    if pyomo.value(sum(model.MLB[(timeStep-1, sLB, e)] for e in model.element)) != 0:
                        MLBtoLC += (model.MLBtoLC[(timeStep, sLB, stockLC)]
                                    *model.MLB[(timeStep-1, sLB, element)]
                                    /sum(model.MLB[(timeStep-1, sLB, e)] 
                                         for e in model.element))
                return (model.MLC[(timeStep-1, stockLC, element)]
                        - MLCtoFP
                        + MLBtoLC)  
            model.ConstraintMassLC = pyomo.Constraint(
                model.computingTime, model.stockLC, model.element, 
                rule = lambda model, timeStep, stockLC, element:
                computeMassLC(model, timeStep, stockLC, element) 
                == model.MLC[(timeStep, stockLC, element)]
            )

            def computeMassFP(model, timeStep, stockFP, element):
                if (timeStep < 4) or (pyomo.value(sum(model.MFP[(timeStep-1, stockFP, e)] 
                                              for e in model.element)) == 0):
                    MfromFP = 0
                else:
                    MfromFP = (model.MfromFP[(timeStep, stockFP)]
                               *model.MFP[(timeStep-1, stockFP, element)]
                               /sum(model.MFP[(timeStep-1, stockFP, e)] for e in model.element))
                MLCtoFP = 0
                for sLC in model.stockLC:
                    if (pyomo.value(sum(model.MLC[(timeStep-1, sLC, e)] 
                                        for e in model.element)) != 0):
                        MLCtoFP += (model.MLCtoFP[(timeStep, sLC, stockFP)]
                                    *model.MLC[(timeStep-1, sLC, element)]
                                    /sum(model.MLC[(timeStep-1, sLC, e)] 
                                         for e in model.element))
                return (model.MFP[(timeStep-1, stockFP, element)]
                        - MfromFP 
                        + MLCtoFP)
            model.ConstraintMassFP = pyomo.Constraint(
                model.computingTime, model.stockFP, model.element, 
                rule = lambda model, timeStep, stockFP, element:
                computeMassFP(model, timeStep, stockFP, element)
                == model.MFP[(timeStep, stockFP, element)]
            )

        elif objective == "MaximizeProfit":
            def computeMassLC(model, timeStep, stockLC, element):
                if (timeStep < 3) or (pyomo.value(sum(model.MLC[(timeStep-1, stockLC, e)]
                                                      for e in model.element)) == 0):
                    MfromLC = 0
                else:
                    MfromLC = (model.MfromLC[(timeStep, stockLC)]
                               *model.MLC[(timeStep-1, stockLC, element)]
                               /sum(model.MLC[(timeStep-1, stockLC, e)] for e in model.element))
                MLBtoLC = 0
                for sLB in model.stockLB:
                    if (pyomo.value(sum(model.MLB[(timeStep-1, sLB, e)]
                                        for e in model.element)) != 0):
                        MLBtoLC += (model.MLBtoLC[(timeStep, sLB, stockLC)]
                                    *model.MLB[(timeStep-1, sLB, element)]
                                    /sum(model.MLB[(timeStep-1, sLB, e)] 
                                        for e in model.element))
                return (model.MLC[(timeStep-1, stockLC, element)]
                        - MfromLC
                        + MLBtoLC)
            model.ConstraintMassLC = pyomo.Constraint(
                model.computingTime, model.stockLC, model.element, 
                rule = lambda model, timeStep, stockLC, element:
                computeMassLC(model, timeStep, stockLC, element)
                == model.MLC[(timeStep, stockLC, element)]
            )


        model = getattr(self, "_objective%s" % objective)(model)
        solver = pyomo.SolverFactory('ipopt')
        solver.options['max_iter'] = max_iter
        solver.options['tol'] = tol
        result = solver.solve(model)
        model.solutions.store_to(result)

        self._retrieveResult(model, objective)


    def schedule(self, objective, outputFileName, outputDirectory, 
                 firstTime = 1, lastTime = 30, 
                 **kwargs):
        if objective == "all":
            objective = ['MaximizeF', 'MaximizeProfit']
        else:
            objective = [objective]
        for obj in objective:
            print("#################################")
            print("###", obj, " - 1st time")
            print("#################################")
            if (obj == 'MaximizeProfit') and (self.price is None):
                raise Exception('Profit table is required.')
            FileName = "%s_%s.xlsx" % (outputFileName, obj) 
            start_time = time.time()
            self._launchOptimization(objective = obj, 
                                     init = False, 
                                     firstTime = firstTime, 
                                     lastTime = lastTime,
                                     **kwargs)
            print("Optimization time: ", time.time() - start_time)
            i = 1
            while self._checkConstraint(objective=obj) == False:
                i += 1
                print("#################################")
                print("###", obj, " - %dth time" % i)
                print("#################################")
                self._launchOptimization(objective = obj, 
                                         init = True, 
                                         firstTime = firstTime, 
                                         lastTime = lastTime,
                                         **kwargs)
                print("Optimization time: ", time.time() - start_time)
            self._makeOutputFile(FileName, outputDirectory, obj)


if __name__ == '__main__':
    factor = 10000
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", 
                        help = 'Ini configuration file')
    args = parser.parse_args()

    config = configparser.ConfigParser()
    config.read(args.config)
    input_file = os.path.join(config['Input']['directory'], 
                              config['Input']['file'])
    if 'command' in config['Input'].keys():
        command_file = os.path.join(config['Input']['directory'], 
                                    config['Input']['command'])
    else:
        command_file = None
    if 'price' in config['Input'].keys():
        price_file = os.path.join(config['Input']['directory'], 
                                   config['Input']['price'])
    else:
        price_file = None
    output_file = config['Output']['file']
    output_directory = config['Output']['directory']

    obj_function = config['Parameter']['objective_function']
    first_time_step = int(config['Parameter']['first_time_step'])
    last_time_step = int(config['Parameter']['last_time_step'])
    shipmentMin = float(config['Parameter']['shipment_min'])/factor
    shipmentMax = float(config['Parameter']['shipment_max'])/factor
    collectionMin = float(config['Parameter']['collection_min'])/factor
    elements = config['Parameter']['elements'].split(',')
    constrained_elements = config['Parameter']['constrained_elements'].split(',')

    print("Read input file", end = "...")
    locations = {}
    for key in config.keys():
        if (key.find('Location ') != -1) or (key == 'Final Product'):
            location = config[key]['longName']
            location_order = int(config[key]['order'])
            locations[location_order] = location
            if (first_time_step == 1) and (location_order != 1):
                boundLocation = (pd.read_excel(input_file, sheet_name=key, 
                                               usecols="A:H")
                                 .rename(columns = str.lower))
                boundLocation = boundLocation.rename(
                    columns=dict(zip(boundLocation.columns, boundLocation.columns.str.replace(' ', '_')))
                )
                boundLocation  = boundLocation.rename(
                    columns = {boundLocation.columns[0] : 'stock%s' % location}
                )
                boundLocation.set_index("stock%s" % location, inplace=True)
                boundLocation.max_capacity_stock = boundLocation.max_capacity_stock/factor
                massLocation = pd.DataFrame(data = np.zeros((len(boundLocation.index), len(elements))), 
                                            index = pd.MultiIndex.from_arrays([[0]*len(boundLocation.index), 
                                                                               boundLocation.index], 
                                                                               names = ['time', 'stock%s' % location]), 
                                            columns = ['m%s' % element for element in elements])
            else:
                boundLocation = (pd.read_excel(input_file, sheet_name=key, 
                                               usecols="F:M", 
                                               index_col=[1])
                                 .rename(columns = str.lower))
                boundLocation = boundLocation.rename(
                    columns = dict(zip(boundLocation.columns, boundLocation.columns.str.replace(' ', '_')))
                )
                boundLocation.index.rename('stock%s' % location, inplace=True)
                boundLocation.max_capacity_stock = boundLocation.max_capacity_stock/factor
                massLocation = (pd.read_excel(input_file, sheet_name=key, 
                                              usecols="A:E", index_col=[0], 
                                              names = ['stock%s' % location, 'mass'] + ['g%s' % element for element in elements]))
                massLocation.index = pd.MultiIndex.from_arrays([[first_time_step-1]*len(boundLocation.index), 
                                                                boundLocation.index], 
                                                                names = ['time', 'stock%s' % location])
                
                for element in elements:
                    if element not in constrained_elements:
                        massLocation['g%s' % element] = massLocation.apply(
                            lambda x: np.nan if x.mass == 0
                            else 100 - x[['g%s' % element for element in constrained_elements]].sum(), 
                            axis = 1
                        )
                    massLocation['m%s' % element] = massLocation.apply(lambda x: x['g%s' % element]/100*x.mass/factor, axis = 1)
                massLocation.drop(columns=['mass'] + ['g%s' % element for element in elements], inplace=True)
            globals()[location] = boundLocation
            globals()['M%s' % location] = massLocation.fillna(0)

    freshTonnes = (pd.read_excel(input_file, 
                              sheet_name=config['freshTonnes']['longName'], 
                              usecols='A:AO')
                .rename(columns=str.lower)
                .set_index('time')
                .multiply(1/factor))
    print("done.")
    
    print("Read command file", end = ": ")
    if command_file is not None:
        print(command_file)
        command = (pd.read_csv(command_file, sep = ";", decimal=".", index_col=[0])
                   .multiply(1/factor))
    else:
        print("there is no command file, it'll be initialized by 0.")
        command = pd.DataFrame(data = np.zeros((last_time_step - first_time_step + 1, len(FP.index))), 
                               columns = FP.index.to_list(), 
                               index = range(first_time_step, last_time_step + 1))
    
    print("Read price file", end = ": ")
    if price_file is not None:
        print(price_file)
        price = (pd.read_excel(price_file, names=["stock", "price"]))
    else:
        price = None
        print("there is no price file.")


    object_dict = {}
    function_dict = {}
    for order in locations.keys():
        location = locations[order]
        object_dict[location] = globals()[location]
        function_dict[location] = globals()['M%s' % location]
    schedule = Schedule(shipmentMax=shipmentMax, shipmentMin=shipmentMin, 
                        collectionMin=collectionMin,
                        freshTonnes=freshTonnes, command=command,
                        factor=factor,
                        price=price,
                        **object_dict)
    start_time = time.time()
    schedule.schedule(obj_function, output_file, output_directory,
                      first_time_step, last_time_step,
                      **function_dict)
    print("Computational time: ", time.time() - start_time)